var fs = require('fs');
var express = require("express");
const path=require("path");
const puppeteer = require("puppeteer");

const base_url=`http://farmacia.tergas.it:8080/`

var app = express();
app.use(express.json({limit: '200mb'}));
app.use(express.urlencoded({     // to support URL-encoded bodies
  limit:'200mb',
  extended: true
}));

app.use(express.static('public'));

var port = process.argv[2]
console.log(port)
if (isNaN(port)) {
  console.log("usage: node index.js <portnumber>")
  return
}

app.listen(port, () => {
 console.log(`Server running on port ${port}`);
});


app.get("/test", async (req, res, next) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('http://farmacia.tergas.it:8080/invoice.html', {
    waitUntil: 'networkidle2',
  });
  try {
       fs.unlinkSync('./public/hn.pdf')
      }
      catch (e) {
        console.log("cannot delete file",e)
      }
  
  const pdf = await page.pdf({
    path: './public/hn.pdf',
    format: 'a4'
    
  });
  res.redirect("http://farmacia.tergas.it:8080/hn.pdf");
  await browser.close();
})


app.post("/", async (req, res, next) => {
  console.log("reciving data");
  const filename = req.body.filename
  const save=req.body.save
  const headerTemplate=  req.body.headerTemplate
  const footerTemplate = req.body.footerTemplate
  const avoidHeaderOnFirstPage=req.body.avoidHeaderOnFirstPage
  const orientationLandscape=req.body.orientationLandscape

  if (filename && filename.indexOf("..")!==-1) {
    console.log(filename.indexOf(".."))
    res.send('what are you doing?')
    return
  }

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  
  // check post integrity
  if (req.body.url) {
    await page.goto(req.body.url, {waitUntil: 'networkidle2',});
  }
  else if (req.body.html) {await page.setContent(req.body.html,{waitUntil: 'networkidle2',})  }
  else {
    res.send("No data!")
    return
  }

  if (save && filename) {
      // remove file if it exists
      try {
        fs.unlinkSync(`./public/${filename}`)
      }
      catch (e) {
        console.log("cannot delete file",e)
      }
    
      await page.pdf({ path: `./public/${filename}`, format: 'a4'
     });
      res.redirect(`${base_url}${filename}`);
  }
  else {
    console.log("generating on the fly")
    
    if (avoidHeaderOnFirstPage) {
      await page.addStyleTag({
        content: "@page:first {margin-top: 0; margin-bottom: 150px;}"
      });
    }
    if (orientationLandscape) {
      await page.addStyleTag({
        content: "@page { size: landscape;}"
      });
    }
    await page.evaluate(() => {
      document.title = 'My new title';
    });
    const pdf = await page.pdf({
      title:"---",
      format: 'a4',
      printBackground: true,     
      headerTemplate: headerTemplate,
      footerTemplate: footerTemplate,
      displayHeaderFooter: true,
       margin: {
          bottom: '250px', // minimum required for footer msg to display
          left: 0,
          right: 35,
          top: 150,
        },
    });
    
    res.contentType("application/pdf");
    res.send(pdf)
  }
  await browser.close();
});

app.get("/", async (req, res, next) => {

})




